<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function encode($data){
    return base64_encode(serialize($data));
}
function decode($data){
    return unserialize(base64_decode($data));
}

$res = '';
$query_get_records_sql = '';
$query_execute = '';

if (isset($_POST['query_get_records_sql'])) {
    $query_get_records_sql = $_POST['query_get_records_sql'];
    if (isset($query_get_records_sql) && substr($query_get_records_sql,0,6) == 'SELECT') {
        // let's go curl
        //https://sa.magistere.fr/ac-versailles/webservice/rest/server.php?wsfunction=local_centraladmin_agent_ping&wstoken=8357fc4700c59a003faff3d04011ffbb&moodlewsrestformat=json
        $ch = curl_init('https://sa.magistere.fr/ac-versailles/webservice/rest/server.php?wsfunction=local_centraladmin_agent_get_records_sql&wstoken=8357fc4700c59a003faff3d04011ffbb&moodlewsrestformat=json');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('query' => encode($query_get_records_sql), 'params'=>encode(array())) ) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
    
        if (!$res) {
            $res = curl_error($ch);
        } else {
            $res = json_decode($res);
            $res->result = decode($res->result);
        }
    }
} else if (isset($_POST['query_execute'])) {
    $query_execute = $_POST['query_execute'];
    $ch = curl_init('https://sa.magistere.fr/ac-versailles/webservice/rest/server.php?wsfunction=local_centraladmin_agent_execute&wstoken=8357fc4700c59a003faff3d04011ffbb&moodlewsrestformat=json');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('query' => encode($query_execute), 'params'=>encode(array())) ) );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
    
        if (!$res) {
            $res = curl_error($ch);
        } else {
            $res = json_decode($res);
            $res->result = decode($res->result);
        }

}


echo '
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'.
'</head>
<body>

<h1> get_records_sql </h1>
<form action="" method="post">
<textarea name="query_get_records_sql" style="width:100%;min-height:221px">'.$query_get_records_sql.'</textarea><br/>
<input type="submit" name="Executer" value="Executer"/>
</form>

<h1> execute </h1>
<form action="" method="post">
<textarea name="query_execute" style="width:100%;min-height:221px">'.$query_execute.'</textarea><br/>
<input type="submit" name="Executer" value="Executer"/>
</form>

<h1> Resultat </h1>
<pre>
'.print_r($res,true).'
</pre>



';