<?php 

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.php');

echo '<pre>##### START SCRIPT'."\n\n";

// Create the webservice user

$userid = $DB->get_field('user', 'id', array('username'=>'centraladmin'));
if (!$userid) {
    echo '#### Creating user centraladmin'."\n";
    $user = new stdClass();
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->username = 'centraladmin';
    $user->mnethostid = 1;
    $user->password = '';
    $user->firstname = 'centraladmin';
    $user->lastname = 'centraladmin';
    $user->email = 'centraladmin@magistere.fr';
    $user->description = 'Ne jamais modifier cet utilisateur';
    
    $userid = $DB->insert_record('user', $user);
} else {
    echo '#### User centraladmin already existing, skipping user creation...'."\n";
}

$roleid = $DB->get_field('role', 'id', array('name'=>'centraladmin'));
if (!$roleid) {
    echo '#### Creating role centraladmin'."\n";
    $role = new stdClass();
    $role->name = 'centraladmin';
    $role->shortname = 'centraladmin';
    $role->description = 'Ne jamais ajouter d\'utilisateur dans ce role';
    $role->sortorder = '99';
    $role->archetype = '';
    
    $roleid = $DB->insert_record('role', $role);
} else {
    echo '#### Role centraladmin already existing, skipping role creation...'."\n";
}

if (!$DB->record_exists('role_context_levels', array('roleid'=>$roleid, 'contextlevel'=> CONTEXT_SYSTEM))) {
    echo '#### Creating role_context_levels for role centraladmin'."\n";
    $role_context = new stdClass();
    $role_context->roleid = $roleid;
    $role_context->contextlevel = CONTEXT_SYSTEM;
    
    $DB->insert_record('role_context_levels', $role_context);
} else {
    echo '#### Role_context_levels for role centraladmin already existing, skipping role_context_levels creation...'."\n";
}
    
if (!$DB->record_exists('role_assignments', array('roleid'=>$roleid, 'contextid'=>1,'userid'=>$userid))) {
    echo '#### Creating role_assignments for role centraladmin and user centraladmin'."\n";
    $role_assignments = new stdClass();
    $role_assignments->roleid = $roleid;
    $role_assignments->contextid = 1;
    $role_assignments->userid = $userid;
    $role_assignments->timemodified = time();
    $role_assignments->modifierid = 2;
    $role_assignments->component = '';
    $role_assignments->itemid = 0;
    $role_assignments->sortorder = 0;
    $DB->insert_record('role_assignments', $role_assignments);
} else {
    echo '#### Role_assignments for role centraladmin already existing, skipping role_assignments creation...'."\n";
}

$service = $DB->get_record('external_services', array('name' => 'local_centraladmin_agent_service'));
if (!$service) {
    echo '#### External_services local_centraladmin_agent_service not existing, please verify your service'."\n";
}

if ($service && !$DB->record_exists('external_services_users', array('userid'=>$userid, 'externalserviceid'=> $service->id))) {
    echo '#### Linking user centraladmin with external service local_centraladmin_agent_service'."\n";
    $external_services_users = new stdClass();
    $external_services_users->userid = $userid;
    $external_services_users->externalserviceid = $service->id;
    $external_services_users->timecreated = time();
    
    $DB->insert_record('external_services_users', $external_services_users);
} else {
    echo '#### User centraladmin already linked with external service local_centraladmin_agent_service, skipping...'."\n";
}


// capa
$matrice = [
    'moodle/webservice:createtoken' => ['centraladmin'],
    'webservice/rest:use' => ['centraladmin']
];

echo '<pre>';

foreach($matrice as $capacity => $roles){
    $capacityRecord = $DB->get_record('capabilities', array('name' => $capacity));
    
    if($capacityRecord === false){
        echo_n('[ERROR] La capacite ' . $capacity . ' n\'existe pas !');
        continue;
    }else{
        //$DB->delete_records('role_capabilities',array('capability'=>$capacity));
        foreach($roles as $role){
            $roleRecord = $DB->get_record('role', array('shortname' => $role));
            $capa = new stdClass();
            
            $capa->roleid = $roleRecord->id;
            $capa->capability = $capacity;
            $capa->permission = 1;
            $capa->modifierid = 2;
            $capa->timemodified = time();
            
            try
            {
                $records = $DB->get_records('role_capabilities', array('capability' => $capacity, 'roleid' => $roleRecord->id));
                if($records){
                    foreach($records as $record){
                        $capa->id = $record->id;
                        $DB->update_record('role_capabilities', $capa);
                        echo_n('Autorisation pour le rôle -'.$roleRecord->name.'- sur la capacité -'.$capacity.'- avec le context -'.$record->contextid.'-');
                    }
                } else {
                    $capa->contextid = 1;
                    $DB->insert_record('role_capabilities',$capa);
                    echo_n('Ajout de la capacité -'.$capacity.'- pour autoriser le rôle -'.$roleRecord->name);
                }
                
            }
            catch (Exception $e){print_r($e); die();}
        }
    }
    echo "\n";
}
    
echo '##### END SCRIPT';

function echo_n($str)
{
    echo $str;
    echo "\n";
}


