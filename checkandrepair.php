<?php 

require_once(dirname(dirname(__DIR__)) . '/config.php');

$key = 'PyESrq1vmVzaBSnN6W1VcPitqw04MT1CEWbU2VvkIVkZh5qpOV';

$pkey = required_param('k', PARAM_ALPHANUMEXT);

if ($key != $pkey){
    die();
}

$user = new stdClass();
$user->auth = 'manual';
$user->confirmed = 1;
$user->suspended = 0;
$user->deleted = 0;
$user->username = 'centraladmin';
$user->mnethostid = 1;
$user->password = '';
$user->firstname = 'centraladmin';
$user->lastname = 'centraladmin';
$user->email = 'centraladmin@magistere.fr';
$user->description = 'Ne jamais modifier cet utilisateur';

$userid = $DB->get_field('user', 'id', array('username'=>'centraladmin'));
if (empty($userid)) {
    $userid = $DB->insert_record('user', $user);
}else{
    $user->id = $userid;
    $DB->update_record('user', $user);
}

$roleid = $DB->get_field('role', 'id', array('name'=>'centraladmin'));
if (!$roleid) {
    $role = new stdClass();
    $role->name = 'centraladmin';
    $role->shortname = 'centraladmin';
    $role->description = 'Ne jamais ajouter d\'utilisateur dans ce role';
    $role->sortorder = '99';
    $role->archetype = '';
    $roleid = $DB->insert_record('role', $role);
}

if (!$DB->record_exists('role_context_levels', array('roleid'=>$roleid, 'contextlevel'=> CONTEXT_SYSTEM))) {
    $role_context = new stdClass();
    $role_context->roleid = $roleid;
    $role_context->contextlevel = CONTEXT_SYSTEM;
    $DB->insert_record('role_context_levels', $role_context);
}
    
if (!$DB->record_exists('role_assignments', array('roleid'=>$roleid, 'contextid'=>1,'userid'=>$userid))) {
    $role_assignments = new stdClass();
    $role_assignments->roleid = $roleid;
    $role_assignments->contextid = 1;
    $role_assignments->userid = $userid;
    $role_assignments->timemodified = time();
    $role_assignments->modifierid = 2;
    $role_assignments->component = '';
    $role_assignments->itemid = 0;
    $role_assignments->sortorder = 0;
    $DB->insert_record('role_assignments', $role_assignments);
}

$service = $DB->get_record('external_services', array('name' => 'local_centraladmin_agent_service'));
if (!$service) {
    retjson(false);
}

if ($service && !$DB->record_exists('external_services_users', array('userid'=>$userid, 'externalserviceid'=> $service->id))) {
    $external_services_users = new stdClass();
    $external_services_users->userid = $userid;
    $external_services_users->externalserviceid = $service->id;
    $external_services_users->timecreated = time();
    $DB->insert_record('external_services_users', $external_services_users);
}


$context = context_system::instance(); // always system
assign_capability('moodle/webservice:createtoken', CAP_ALLOW, $roleid, $context->id, true);
assign_capability('webservice/rest:use', CAP_ALLOW, $roleid, $context->id, true);

retjson(true);


function retjson($success){
    $json = new stdClass();
    $json->success = $success;
    return json_encode($json);
}
