<?php

defined('MOODLE_INTERNAL') || die();

$services = array(
    'local_centraladmin_agent_service' => array(                                                //the name of the web service
        'functions' => array (
            'local_centraladmin_agent_pair',
            'local_centraladmin_agent_unpair',
            'local_centraladmin_agent_get_records_sql',
            'local_centraladmin_agent_execute',
            'local_centraladmin_agent_ping',
            'local_centraladmin_agent_get_archetype_permissions',
            'local_centraladmin_agent_update_permission',
            'local_centraladmin_agent_compare_permissions',
        ), //web service functions of this service
        'requiredcapability' => '',                //if set, the web service user need this capability to access
                                                                      //any function of this service. For example: 'some/capability:specified'
        'restrictedusers' =>1,                                             //if enabled, the Moodle administrator must link some user to this service
                                                                      //into the administration
        'enabled'=>1,                                                       //if enabled, the service can be reachable on a default installation
        'shortname' => 'local_centraladmin_agent_service'
    )
);

$functions = array(
    'local_centraladmin_agent_pair' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'pair',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Pair the master with the slave',
        'type'        => 'write'
    ),
    'local_centraladmin_agent_unpair' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'unpair',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Unpair the master and slave',
        'type'        => 'write'
    ),
    'local_centraladmin_agent_get_records_sql' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'get_records_sql',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Return the result of a sql query.',
        'type'        => 'read',
    ),
    'local_centraladmin_agent_execute' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'execute',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Execute a sql query.',
        'type'        => 'write',
    ),
    'local_centraladmin_agent_ping' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'ping',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Ping',
        'type'        => 'read',
    ),
    'local_centraladmin_agent_get_archetype_permissions' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'get_archetype_permissions',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Get permissions of all the archetypes',
        'type'        => 'read',
    ),
    'local_centraladmin_agent_update_permission' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'update_permission',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Update a permission',
        'type'        => 'write',
    ),
    'local_centraladmin_agent_compare_permissions' => array(
        'classname'   => 'local_centraladmin_agent_external',
        'methodname'  => 'compare_permissions',
        'classpath'   => 'local/centraladmin_agent/externallib.php',
        'description' => 'Compare all permission and return differences',
        'type'        => 'read',
    ),
);