<?php
/**
 * This file keeps track of upgrades to
 * the centraladmin module
 *
 * Sometimes, changes between versions involve
 * alterations to database structures and other
 * major things that may break installations.
 *
 * The upgrade function in this file will attempt
 * to perform all the necessary actions to upgrade
 * your older installation to the current version.
 *
 * If there's something it cannot do itself, it
 * will tell you what you need to do.
 *
 * The commands in here will all be database-neutral,
 * using the methods of database_manager class
 *
 * Please do not forget to use upgrade_set_timeout()
 * before any action that may take longer time to finish.
 *
 * @package   centraladmin_agent
 * @copyright 2021 TCS
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_centraladmin_agent_upgrade($oldversion) {

    global $DB;

    $dbman = $DB->get_manager();

    if($oldversion < 2021072204) {
        // Define table local_coursehub_course to be created
        $tablename = 'centraladmin_agent_dayacces';
        $table = new xmldb_table($tablename);

        if (!$dbman->table_exists($table)) {
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
            $table->add_field('daytime', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);
            $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);
            $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);
            $table->add_field('roleid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);

            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);

            $dbman->create_table($table);
        }
        
        upgrade_plugin_savepoint(true, 2021072204, 'local', 'centraladmin_agent');
    }

    return true;
}
