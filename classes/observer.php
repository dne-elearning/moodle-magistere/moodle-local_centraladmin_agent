<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observers used in local entraladmin_agent.
 *
 * @package    local_centraladmin_agent
 * @copyright  2021 TCS
 * @author     TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


/**
 * Event observer for local_myindex.
 */
class local_centraladmin_agent_observer {
    
    /**
     * Triggered via core\event\course_viewed event.
     *
     * @param core\event\course_viewed $event
     */
    public static function course_viewed(core\event\course_viewed $event) {
        global $DB;

        $timeday = mktime(12,0,0);
        // get the roles for the user and its courses
        $context = context_course::instance($event->courseid);
        $roles = get_user_roles($context, $event->userid, false);

        foreach($roles as $role) {
            $dayaccess = $DB->record_exists('centraladmin_agent_dayacces', array('userid'=>$event->userid, 'courseid'=>$event->courseid, 'daytime'=>$timeday ,'roleid'=>$role->roleid));
            if ($dayaccess === false) {
                // Insert new dayaccess for the course
                $last = new stdClass();
                $last->userid     = $event->userid;
                $last->courseid   = $event->courseid;
                $last->daytime = $timeday;
                $last->roleid = $role->roleid;
                $DB->insert_record('centraladmin_agent_dayacces', $last);
            } 
        
        }
    }
    
}
