# moodle-local_centraladmin_agent

Plugin agent pour le plugin _centraladmin_ installé sur la plateforme de monitoring _pilotage_. Ce plugin doit être installé sur les instances de Magistère.

## Démarrage

Remplir le champ _$user->password_ dans les fichiers _script/install_centraladmin_agent.php_ et checkandrepair.php.
Ce champ doit être rempli avec le hash du mot de passe souhaité.

Le script _script/install_centraladmin_agent_ permet de créer l'utilisateur, le rôle, le service et d'attribuer les bonnes autorisations pour que la connexion puisse correctement se faire avec le plugin _centraladmin_ installé sur la plateforme _pilotage_.

