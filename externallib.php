<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/externallib.php');

class local_centraladmin_agent_external extends external_api {

    public const PLUGIN_NAME = 'local_centraladmin_agent';

    public static function ping_parameters() {
        return new external_function_parameters(array());
    }

    public static function ping_returns() {
        return new external_single_structure(
            array(
                'ping' => new external_value(PARAM_ALPHA, '')
            )
        );
    }

    public static function ping() {
        return array('ping' => 'pong');
    }

    public static function get_records_sql_parameters() {
        return new external_function_parameters(array(
            'query' => new external_value(PARAM_RAW, ''),
            'params' => new external_value(PARAM_RAW, ''),
        ));
    }

    public static function get_records_sql_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_BOOL, ''),
                'errormsg' => new external_value(PARAM_TEXT, ''),
                'count' => new external_value(PARAM_INT, ''),
                'result' => new external_value(PARAM_RAW, ''), // base64 with serialization before
            )
        );
    }

    public static function get_records_sql($query, $params) {
        global $DB;

        $res = new stdClass();

        try {
            // unserialize
            $sql = self::replace_placeholder(self::decode($query));
            $params = self::decode($params);
            
            $data = $DB->get_records_sql($sql, $params);
            $res->error = false;
            $res->errormsg = '';
            $res->count = count($data);
            $res->result = self::encode($data);
            
        } catch(dml_exception | Exception $e) {
            $res->error = true;
            $res->errormsg = $e->getCode()." : ".$e->getMessage()."\n\n". $e->getTraceAsString();
            $res->count = 0;
            $res->result = '';
        }

        return $res;
    }

    public static function execute_parameters() {
        return new external_function_parameters(array(
            'query' => new external_value(PARAM_RAW, ''),
            'params' => new external_value(PARAM_RAW, ''),
        ));
    }

    public static function execute_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_BOOL, ''),
                'errormsg' => new external_value(PARAM_TEXT, ''),
            )
        );
    }

    public static function execute($query, $params) {
        global $DB;

        $res = new stdClass();
        
        try {
            // unserialize
            $sql = self::replace_placeholder(self::decode($query));
            $params = self::decode($params);
            
            $DB->execute($sql, $params);
            $res->error = false;
            $res->errormsg = '';
            
        } catch(dml_exception | Exception $e) {
            $res->error = true;
            $res->errormsg = $e->getCode()." : ".$e->getMessage()."\n\n". $e->getTraceAsString();
        }

        return $res;
    }

    public static function pair_parameters() {
        return new external_function_parameters(array(
            'url' => new external_value(PARAM_URL, ''),
            'username' => new external_value(PARAM_USERNAME, ''),
            'password' => new external_value(PARAM_RAW, ''),
            'servicename' => new external_value(PARAM_ALPHANUMEXT, ''),
        ));
    }

    public static function pair_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_BOOL, ''),
                'errormsg' => new external_value(PARAM_TEXT, ''),
                'name' => new external_value(PARAM_TEXT, ''),
            )
        );
    }

    public static function pair($url, $username, $password, $servicename) {
        global $CFG;

        $query = new stdClass();
        $query->username = $username;
        $query->password = $password; 
        $query->service = $servicename;

        $res = new stdClass();
        $res->error = false;
        $res->errormsg = '';

        if (isset($CFG->academie_name)) {
            $res->name = $CFG->academie_name;
        } else {
            $site = get_site();
            $res->name = $site->shortname; 
        }

        list($error, $resToken) = self::post(self::get_token_url($url), $query);

        if ($error) {
            $res->error = true;
            $res->errormsg = $resToken;
            return $res;
        }

        $decodedTokenRes = json_decode($resToken);
        if ($decodedTokenRes === null) {
            $res->error = true;
            $res->errormsg = "SLAVE:json cannot be decoded for payload \n#######\n".$resToken."\n#######";
            return $res;
        } else if(isset($decodedTokenRes->error)) {
            $res->error = true;
            $res->errormsg = "SLAVE:API returned an error \n#######\n".$resToken."\n#######";
            return $res;
        }

        $plugin = self::PLUGIN_NAME;
        set_config('master_url', $url, $plugin);
        set_config('master_username', $username, $plugin);
        set_config('master_password', $password, $plugin);
        set_config('master_servicename', $servicename, $plugin);

        set_config('token', $decodedTokenRes->token, $plugin);
        set_config('privatetoken', $decodedTokenRes->privatetoken, $plugin);

        return $res;
    }


    public static function unpair_parameters() {
        return new external_function_parameters(array(
            'url' => new external_value(PARAM_URL, ''),
        ));
    }

    public static function unpair_returns() {
        return new external_single_structure(
            array(
                'error' => new external_value(PARAM_BOOL, ''),
                'errormsg' => new external_value(PARAM_TEXT, ''),
            )
        );
    }

    public static function unpair($url) {
        $res = new stdClass();
        if (get_config(self::PLUGIN_NAME, 'master_url') !== $url) {
            $res->error = true;
            $res->errormsg = 'SLAVE:API master url not matching existing one ("'.get_config(self::PLUGIN_NAME, 'master_url').'" != "'.$url.'")';
            return $res;
        }

        // unset config
        $conf = get_config(self::PLUGIN_NAME);
        foreach ($conf AS $key=>$c) {
            if ($key != 'version'){
                set_config($key, null, self::PLUGIN_NAME);
            }
        }

        $res->error = false;
        $res->errormsg = "";
        return $res;
    }

    private static function encode($data){
        return base64_encode(serialize($data));
    }
    private static function decode($data){
        return unserialize(base64_decode($data));
    }

    private static function get_token_url($baseurl){
        return $baseurl.'/login/token.php';
    }

    private static function get_webservice_url($baseurl){
        return $baseurl.'/webservice/rest/server.php';
    }

    private static function post($url, $data, $refUrl='', $header=true)
    {
        global $CFG;
        $cprocess = curl_init($url);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($cprocess, CURLOPT_POST, true);
        curl_setopt($cprocess, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cprocess, CURLOPT_REFERER, $refUrl);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cprocess, CURLOPT_POSTFIELDS, $data);

        if (!empty($CFG->proxyhost) && !is_proxybypass($url)) {
            if ($CFG->proxyport === '0') {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost);
            } else {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost.':'.$CFG->proxyport);
            }
        }

        $res = curl_exec($cprocess);
        if (!$res) {
            $returnArray = array(true, curl_errno($cprocess).' : '.curl_error($cprocess)); 
        } else {
            $returnArray = array(false, $res);
        }
        
        curl_close($cprocess);
        return $returnArray;
    }

    private static function replace_placeholder($subject) {
        global $CFG;

        $replacedSubject = str_replace(array('[[[TABLE_PREFIX]]]', '[[[DB_CENTRAL]]]', '[[[DB_PREFIX]]]'), array($CFG->prefix, $CFG->centralized_dbname, $CFG->db_prefix), $subject);
        $regex = '/\[\[\[MAGDB_([a-z-]+?)\]\]\]/m';

        $replacedSubject = preg_replace_callback(
            $regex,
            function($matches) {
                global $CFG;
                if (isset(get_magistere_academy_config()[$matches[1]])) {
                    $acaname = $CFG->db_prefix.get_magistere_academy_config()[$matches[1]]['shortname'];
                } else {
                    throw new Exception('Invalid placeholder : aca named '.$matches[1].' does not exist.');
                }
                return $acaname;
            },
            $replacedSubject
        );

        return $replacedSubject;
    }

    
    public static function get_archetype_permissions_parameters() {
        return new external_function_parameters(array());
    }

    public static function get_archetype_permissions_returns() {
        return new external_single_structure(
            array(
                'permissions' => new external_value(PARAM_RAW, ''), // base64 with serialization before
            )
        );
    }

    public static function get_archetype_permissions() {
        $perms = array();
        $archetypes = get_role_archetypes();
        foreach($archetypes as $archetype) {
            $perms[$archetype] = get_default_capabilities($archetype); 
        }

        $res = new stdClass();
        $res->permissions = self::encode($perms);
        return $res;
    }
    
    
    public static function update_permission_parameters() {
        return new external_function_parameters(array(
            'capability' => new external_value(PARAM_CAPABILITY, ''),
            'permission' => new external_value(PARAM_PERMISSION, ''),
            'roleshortname' => new external_value(PARAM_RAW, ''),
        ));
    }
    
    public static function update_permission_returns() {
        return new external_single_structure(
            array(
                'result'=> new external_value(PARAM_BOOL, ''),
                'msg'=> new external_value(PARAM_BOOL, '', VALUE_OPTIONAL),
            )
        );
    }
    
    public static function update_permission($capability, $permission, $roleshortname) {
        global $DB;
        $context = context_system::instance(); // always system
        $role = $DB->get_record('role', array('shortname' => $roleshortname));
        $res = new StdClass();
        $res->result = true;
        try {
            assign_capability($capability, $permission, $role->id, $context->id, true);
        }catch (Exception $e) {
            $res->result = false;
            $res->msg = $e->getMessage()."\n\n".$e->getTraceAsString();
        }
        return $res;
    }
    
    
    public static function compare_permissions_parameters() {
        return new external_function_parameters(array(
            'permissions' => new external_value(PARAM_RAW, '')
        ));
    }
    
    public static function compare_permissions_returns() {
        return new external_single_structure(
            array(
                'result'=> new external_value(PARAM_RAW, ''),
            )
        );
    }
    
    public static function compare_permissions($permissions) {
        global $DB;
        
        $data = new stdClass();
        $data->insert = array();
        $data->update = array();
        $data->delete = array();
        
        $permissions = unserialize(gzuncompress(base64_decode($permissions)));
        
        $sql = 'SELECT
CONCAT(r.id,"|",c.name) as uid,
c.name AS capability,
r.id AS roleid,
IF(rc.permission IS NULL,0,rc.permission) AS permission,
IF(rc.timemodified IS NULL,0,rc.timemodified) AS timemodified,
r.name AS rolename,
r.shortname AS roleshortname,
r.archetype
FROM {capabilities} c
INNER JOIN {role} r
LEFT JOIN {role_capabilities} rc ON c.name = rc.capability AND rc.roleid = r.id AND rc.contextid = ?';
        
        $perms = $DB->get_records_sql($sql,array(1));
        
        foreach($perms AS $perm) {
            if (array_key_exists($perm->uid, $permissions)){
                if ($permissions[$perm->uid] != $perm->permission){
                    $data->update[] = $perm;
                    unset($permissions[$perm->uid]);
                }else{
                    unset($permissions[$perm->uid]);
                    continue;
                }
            }else{
                $data->insert[] = $perm;
            }
        }
        $data->delete = $permissions;
        
        $res  = new StdClass();
        $res->result = self::encode($data);
        return $res;
    }
    
    
    
    

}

